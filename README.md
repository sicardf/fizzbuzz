# Fizz Buzz

![Platform](https://img.shields.io/badge/plateforme-ios-lightgrey.svg?style=flat) ![Swift](https://img.shields.io/badge/in-Swift%205-orange.svg?style=flat)

## Description
A simple fizz-buzz application. Architcture MVVM-C

## Run
**Fizz Buzz** use cocoapods for dependency management.

Run the following command :

```ruby

$ pod install

```

## Project implementation

#### Requirements

- iOS 14.5+

- Xcode 12.5+

- Swift 5.0+

#### Library

- [SwiftLint](https://github.com/realm/SwiftLint) - Use of a linter to ensure good code quality

- [Charts](https://github.com/danielgindi/Charts) - Use of a graphics library to avoid long development for a complex element

#### To do

- Benchmark for a more optimized fizzbuzz generation / Memory optimization
- Implementation of functionals tests
- Optimization of the tableview reload (with a difference check between two models)
- Adding an icon and a splash screen / design system
- Better management of errors / displays
- Add a scrollview for the form and better manage small screen