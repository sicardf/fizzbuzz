//
//  CustomTextField.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

class CustomTextField: UITextField {
    
    let textPadding = UIEdgeInsets(
        top: Grid.spacing10.cgFloat,
        left: Grid.spacing20.cgFloat,
        bottom: Grid.spacing10.cgFloat,
        right: Grid.spacing20.cgFloat
    )

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = frame.size.height / 2
        backgroundColor = Color.panel.color
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)

        return rect.inset(by: textPadding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)

        return rect.inset(by: textPadding)
    }
}
