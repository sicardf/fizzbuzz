//
//  Color.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

enum Color: String {
    case background
    case panel
    case primary
    case white
    
    var color: UIColor? {
        return UIColor(named: self.rawValue)
    }
}
