//
//  Grid.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

enum Grid: CGFloat {
    case spacing0 = 0.0
    case spacing4 = 4.0
    case spacing8 = 8.0
    case spacing10 = 10.0
    case spacing12 = 12.0
    case spacing16 = 16.0
    case spacing20 = 20.0
    case spacing24 = 24.0
    case spacing36 = 36.0
    case spacing48 = 48.0
    case spacing64 = 64.0
    case spacing96 = 96.0
    case spacing144 = 144
    
    var cgFloat: CGFloat {
        return self.rawValue
    }
}
