//
//  Coordinator.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

protocol Coordinator {

    func start()
    func coordinate(to coordinator: Coordinator)
}

extension Coordinator {

    func coordinate(to coordinator: Coordinator) {
        coordinator.start()
    }
}
