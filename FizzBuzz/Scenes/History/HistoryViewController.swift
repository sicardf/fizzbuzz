//
//  HistoryViewController.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit
import Charts

class HistoryViewController: UIViewController {

    lazy var chartView: BarChartView = {
        let barChartView = BarChartView()
        
        barChartView.rightAxis.enabled = false
        barChartView.leftAxis.enabled = false
        barChartView.legend.enabled = false
        barChartView.xAxis.enabled = false
        barChartView.scaleXEnabled = false
        barChartView.scaleYEnabled = false
        barChartView.chartDescription?.enabled = false
        barChartView.noDataText = "No data available"
        barChartView.backgroundColor = Color.panel.color
        barChartView.layer.cornerRadius = Grid.spacing12.cgFloat
        barChartView.layer.masksToBounds = true
        
        barChartView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [barChartView.heightAnchor.constraint(equalToConstant: Grid.spacing144.cgFloat)])

        return barChartView
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        return tableView
    }()
    
    var coordinator: HistoryFlow?
    var viewModel: HistoryViewModelProtocol = HistoryViewModel()
    var tableViewData: [HistoryModel]? {
        didSet {
            tableView.reloadData()
        }
    }

    let cellReuseIdentifier = "cell"

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.fetchHistory()
    }
}

private extension HistoryViewController {

    func setup() {
        setupView()
        setupNavigationBar()
        setupBarChart()
        setupTableView()
        setupViewModel()
    }

    func setupView() {
        title = "History"
        view.backgroundColor = Color.background.color
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func setupBarChart() {
        view.addSubview(chartView)
        
        NSLayoutConstraint.activate(
            [chartView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
             chartView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Grid.spacing16.cgFloat),
             view.trailingAnchor.constraint(equalTo: chartView.trailingAnchor, constant: Grid.spacing16.cgFloat)])
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate(
            [tableView.topAnchor.constraint(equalTo: chartView.bottomAnchor, constant: Grid.spacing10.cgFloat),
             tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
             tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
             tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)])
    }
    
    func setupViewModel() {
        viewModel.tableViewDataDidChange = { [weak self] (tableViewData) in
            self?.tableViewData = tableViewData
        }
        
        viewModel.graphDataDidChange = { [weak self] (chartData) in
            
            self?.chartView.data = chartData
        }
    }
}

private extension HistoryViewController {
    
    @objc func closeAction() {
        dismiss(animated: true, completion: nil)
    }
}

extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "TOP 10"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Grid.spacing36.cgFloat
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) else {
            return UITableViewCell()
        }

        cell.textLabel?.text = tableViewData?[indexPath.row].description
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let fizzBuzzModel = tableViewData?[indexPath.row].fizzBuzzModel {
            coordinator?.coordinateToResult(with: fizzBuzzModel)
        }
    }
}
