//
//  HistoryCoordinator.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

protocol HistoryFlow: AnyObject {
    
    func coordinateToResult(with model: FizzBuzzModel)
}

class HistoryCoordinator: Coordinator {

    weak var navigationController: UINavigationController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let historyViewController = HistoryViewController()

        historyViewController.coordinator = self

        navigationController?.pushViewController(historyViewController, animated: false)
    }
}

extension HistoryCoordinator: HistoryFlow {
    
    func coordinateToResult(with model: FizzBuzzModel) {
        let resultCoordinator = ResultCoordinator(
            navigationController: navigationController,
            fizzBuzzModel: model)

        coordinate(to: resultCoordinator)
    }
}
