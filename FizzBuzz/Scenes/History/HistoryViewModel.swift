//
//  HistoryViewModel.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import Foundation
import Charts

protocol HistoryViewModelProtocol {
    
    var tableViewDataDidChange: (([HistoryModel]) -> Void)? { get set }
    var graphDataDidChange: ((BarChartData?) -> Void)? { get set }
    
    func fetchHistory()
}

class HistoryViewModel: HistoryViewModelProtocol {
    
    var tableViewDataDidChange: (([HistoryModel]) -> Void)?
    var graphDataDidChange: ((BarChartData?) -> Void)?
    
    let fizzBuzzGenerate = FizzBuzzManager()
    
    var tableViewData = [HistoryModel]() {
        didSet {
            tableViewDataDidChange?(tableViewData)
        }
    }
    
    var chartData: BarChartData? {
        didSet {
            graphDataDidChange?(chartData)
        }
    }
}

extension HistoryViewModel {

    func fetchHistory() {
        guard let fizzBuzzSearchEntity = fetchFizzBuzzSearchEntity() else {
            tableViewData = []
            chartData = nil
            
            return
        }
        
        var historyModel = [HistoryModel]()
        
        fizzBuzzSearchEntity.enumerated().forEach({ elem in
            if let fizzBuzzModel = elem.element.value.first {
                historyModel.append(
                    HistoryModel(
                        count: elem.element.value.count,
                        fizzBuzzModel: fizzBuzzModel,
                        description: String(
                            format: "%d : %@",
                            elem.offset + 1,
                            fizzBuzzModel.format)))
            }
        })
        
        tableViewData = historyModel
        
        var dataEntries: [BarChartDataEntry] = []
        
        tableViewData.enumerated().forEach {
            let dataEntry = BarChartDataEntry(x: Double($0.offset), y: Double($0.element.count))
            
            dataEntries.append(dataEntry)
        }
        
        let set = BarChartDataSet(entries: dataEntries)
        set.setColor(NSUIColor(cgColor: Color.primary.color?.cgColor ?? UIColor.blue.cgColor))
        
        chartData = BarChartData(dataSet: set)
    }
}

private extension HistoryViewModel {
    
    func fetchFizzBuzzSearchEntity() -> [Dictionary<String?, [FizzBuzzModel]>.Element]? {
        do {
            let fizzBuzzSearch = try CoreDataManager.shared.getFizzBuzzSearchEntity()
            
            return Array((Dictionary(grouping: fizzBuzzSearch, by: { $0?.format })
                        .sorted { $0.1.count > $1.1.count })[...9])
        } catch {
            return nil
        }
    }
}
