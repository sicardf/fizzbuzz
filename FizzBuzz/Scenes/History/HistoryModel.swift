//
//  HistoryModel.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import Foundation

struct HistoryModel {

    var count: Int
    var fizzBuzzModel: FizzBuzzModel
    var description: String
}
