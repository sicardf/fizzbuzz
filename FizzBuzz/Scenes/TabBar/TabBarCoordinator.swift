//
//  TabBarCoordinator.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

class TabBarCoordinator: Coordinator {

    let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let tabBarController = TabBarController()
        tabBarController.coordinator = self

        let formNavigationController = UINavigationController()
        let formCoordinator = FormCoordinator(navigationController: formNavigationController)
        formNavigationController.tabBarItem = UITabBarItem(
            tabBarSystemItem: .search,
            tag: 0)

        let historyNavigationController = UINavigationController()
        let historyCoordinator = HistoryCoordinator(navigationController: historyNavigationController)
        historyNavigationController.tabBarItem = UITabBarItem(
            tabBarSystemItem: .history,
            tag: 1)

        tabBarController.viewControllers = [formNavigationController,
                                            historyNavigationController]

        tabBarController.modalPresentationStyle = .fullScreen
        navigationController.present(tabBarController, animated: false, completion: nil)

        coordinate(to: formCoordinator)
        coordinate(to: historyCoordinator)
    }
}
