//
//  TabBarController.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

class TabBarController: UITabBarController {

    var coordinator: TabBarCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
}

private extension TabBarController {
    
    func setup() {
        setupTabBar()
    }
    
    func setupTabBar() {
        tabBar.layer.masksToBounds = true
        tabBar.tintColor = Color.primary.color
    }
}
