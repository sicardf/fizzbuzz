//
//  ResultCoordinator.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

protocol ResultFlow: AnyObject {

    func dismissResult()
}

class ResultCoordinator: Coordinator {

    let navigationController: UINavigationController
    let fizzBuzzModel: FizzBuzzModel

    init(navigationController: UINavigationController?,
         fizzBuzzModel: FizzBuzzModel) {
        self.navigationController = navigationController ?? UINavigationController()
        self.fizzBuzzModel = fizzBuzzModel
    }

    func start() {
        let resultViewController = ResultViewController()

        resultViewController.coordinator = self
        resultViewController.fizzBuzzModel = fizzBuzzModel

        navigationController.present(
            UINavigationController(rootViewController: resultViewController),
            animated: true,
            completion: nil)
    }
}

extension ResultCoordinator: ResultFlow {
    
    func dismissResult() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
