//
//  ResultViewModel.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import Foundation

protocol ResultViewModelProtocol {
    
    var dataDidChange: ((String?) -> Void)? { get set }
    
    func generate(model: FizzBuzzModel)
}

class ResultViewModel: ResultViewModelProtocol {

    var dataDidChange: ((String?) -> Void)?
    
    let fizzBuzzGenerate = FizzBuzzManager()

    var data: String = "" {
        didSet {
            dataDidChange?(data)
        }
    }
}

extension ResultViewModel {
    
    func generate(model: FizzBuzzModel) {
        self.data = fizzBuzzGenerate.generateWith(model).joined(separator: ",")
    }
}
