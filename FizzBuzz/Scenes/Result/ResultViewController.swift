//
//  ResultViewController.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

class ResultViewController: UIViewController {

    lazy var resultTextView: UITextView = {
        let textView = UITextView()
        
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = false

        return textView
    }()

    var coordinator: ResultFlow?
    var fizzBuzzModel: FizzBuzzModel?
    var viewModel: ResultViewModelProtocol = ResultViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        generate()
    }
}

private extension ResultViewController {

    func setup() {
        setupView()
        setupNavigationBar()
        setupTextView()
        setupViewModel()
    }

    func setupView() {
        title = "Result"
        view.backgroundColor = Color.background.color
    }
    
    func setupNavigationBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .close,
            target: self,
            action: #selector(closeAction))
    }

    func setupTextView() {
        view.addSubview(resultTextView)
        
        NSLayoutConstraint.activate(
            [resultTextView.topAnchor.constraint(equalTo: view.topAnchor),
             resultTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
             resultTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
             resultTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor)])
    }

    func setupViewModel() {
        viewModel.dataDidChange = { [weak self] (result) in
            self?.resultTextView.text = result
        }
    }
}

private extension ResultViewController {
    
    func generate() {
        guard let fizzBuzzModel = fizzBuzzModel else { return }
        
        viewModel.generate(model: fizzBuzzModel)
    }
}

private extension ResultViewController {
    
    @objc func closeAction() {
        dismiss(animated: true, completion: nil)
    }
}
