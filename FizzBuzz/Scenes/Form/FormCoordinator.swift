//
//  FormCoordinator.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

protocol FormFlow: AnyObject {

    func coordinateToResult(with model: FizzBuzzModel)
    func coordinateToAlert()
}

class FormCoordinator: Coordinator {

    weak var navigationController: UINavigationController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let formViewController = FormViewController()

        formViewController.coordinator = self

        navigationController?.pushViewController(formViewController, animated: false)
    }
}

extension FormCoordinator: FormFlow {
    
    func coordinateToResult(with model: FizzBuzzModel) {
        let resultCoordinator = ResultCoordinator(
            navigationController: navigationController,
            fizzBuzzModel: model)

        coordinate(to: resultCoordinator)
    }
    
    func coordinateToAlert() {
        let alert = UIAlertController(
            title: "Error",
            message: "The form is not valid",
            preferredStyle: .alert)

        alert.addAction(
            UIAlertAction(
                title: "OK",
                style: .destructive,
                handler: nil))

        navigationController?.present(alert, animated: true)
    }
}
