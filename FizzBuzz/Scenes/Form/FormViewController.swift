//
//  FormViewController.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit

class FormViewController: UIViewController {

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()

        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Grid.spacing8.cgFloat
        stackView.translatesAutoresizingMaskIntoConstraints = false

        return stackView
    }()

    lazy var int1TextField: UITextField = {
        let textField = CustomTextField()

        textField.keyboardType = .numberPad
        textField.placeholder = "int1"
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [textField.heightAnchor.constraint(equalToConstant: Grid.spacing36.cgFloat)])

        return textField
    }()

    lazy var int2TextField: UITextField = {
        let textField = CustomTextField()

        textField.keyboardType = .numberPad
        textField.placeholder = "int2"
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [textField.heightAnchor.constraint(equalToConstant: Grid.spacing36.cgFloat)])

        return textField
    }()

    lazy var limitTextField: UITextField = {
        let textField = CustomTextField()

        textField.keyboardType = .numberPad
        textField.placeholder = "limit"
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [textField.heightAnchor.constraint(equalToConstant: Grid.spacing36.cgFloat)])

        return textField
    }()

    lazy var str1TextField: UITextField = {
        let textField = CustomTextField()

        textField.keyboardType = .default
        textField.placeholder = "str1"
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [textField.heightAnchor.constraint(equalToConstant: Grid.spacing36.cgFloat)])

        return textField
    }()

    lazy var str2TextField: UITextField = {
        let textField = CustomTextField()

        textField.keyboardType = .default
        textField.placeholder = "str2"
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [textField.heightAnchor.constraint(equalToConstant: Grid.spacing36.cgFloat)])

        return textField
    }()

    lazy var generateButton: UIButton = {
        let button = UIButton()

        button.setTitle("Generate", for: .normal)
        button.setTitleColor(Color.white.color, for: .normal)
        button.backgroundColor = Color.primary.color
        button.layer.cornerRadius = Grid.spacing48.cgFloat / 2
        button.addTarget(
            self,
            action: #selector(showResultTapped),
            for: .touchUpInside)
        button.contentEdgeInsets = UIEdgeInsets(
            top: Grid.spacing0.cgFloat,
            left: Grid.spacing20.cgFloat,
            bottom: Grid.spacing0.cgFloat,
            right: Grid.spacing20.cgFloat)
        button.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [button.heightAnchor.constraint(equalToConstant: Grid.spacing48.cgFloat)])

        return button
    }()

    var coordinator: FormFlow?
    var viewModel: FormViewModelProtocol = FormViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
}

private extension FormViewController {

    func setup() {
        setupView()
        setupNavigationBar()
        setupStackView()
        setupForm()
        setupGenerateButton()
    }

    func setupView() {
        title = "Generate"

        view.backgroundColor = Color.background.color

        hideKeyboardWhenTappedAround()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    func setupStackView() {
        view.addSubview(stackView)

        NSLayoutConstraint.activate(
            [stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Grid.spacing20.cgFloat),
             stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Grid.spacing20.cgFloat),
             view.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor, constant: Grid.spacing20.cgFloat)])
    }

    func setupForm() {
        stackView.addArrangedSubview(int1TextField)
        stackView.addArrangedSubview(int2TextField)
        stackView.addArrangedSubview(limitTextField)
        stackView.addArrangedSubview(str1TextField)
        stackView.addArrangedSubview(str2TextField)
    }
    
    func setupGenerateButton() {
        view.addSubview(generateButton)
        
        NSLayoutConstraint.activate(
            [generateButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: Grid.spacing20.cgFloat),
             generateButton.centerXAnchor.constraint(equalTo: stackView.centerXAnchor)])
    }
}

private extension FormViewController {

    @objc func showResultTapped(_ sender: UIButton) {
        guard let fizzBuzzModel = FizzBuzzModel(
                end: limitTextField.text,
                int1: int1TextField.text,
                int2: int2TextField.text,
                str1: str1TextField.text,
                str2: str2TextField.text),
              viewModel.validForm(model: fizzBuzzModel) else {
            coordinator?.coordinateToAlert()

            return
        }

        CoreDataManager.shared.saveFizzBuzzSearchEntity(with: fizzBuzzModel)
        
        coordinator?.coordinateToResult(with: fizzBuzzModel)
    }
}
