//
//  FormViewModel.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import Foundation

protocol FormViewModelProtocol {
    
    func validForm(model: FizzBuzzModel?) -> Bool
}

class FormViewModel: FormViewModelProtocol {

    func validForm(model: FizzBuzzModel?) -> Bool {
        guard let model = model,
              model.start > 0,
              model.end >= model.start,
              model.int1 > 0,
              model.int2 > 0,
              model.int2 != model.int1,
              !model.str1.isEmpty,
              !model.str2.isEmpty else {
            return false
        }

        return true
    }
}
