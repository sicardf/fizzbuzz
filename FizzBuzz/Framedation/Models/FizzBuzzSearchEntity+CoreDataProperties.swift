//
//  FizzBuzzSearchEntity+CoreDataProperties.swift
//  
//
//  Created by Flavien SICARD on 28/11/2021.
//
//

import Foundation
import CoreData


extension FizzBuzzSearchEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FizzBuzzSearchEntity> {
        return NSFetchRequest<FizzBuzzSearchEntity>(entityName: "FizzBuzzSearchEntity")
    }

    @NSManaged public var int1: Int32
    @NSManaged public var int2: Int32
    @NSManaged public var str1: String?
    @NSManaged public var str2: String?
    @NSManaged public var start: Int32
    @NSManaged public var end: Int32

}
