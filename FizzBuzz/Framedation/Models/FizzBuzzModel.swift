//
//  FizzBuzzModel.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 29/11/2021.
//

import Foundation

struct FizzBuzzModel {

    var start: Int = 1
    var end: Int
    var int1: Int
    var int2: Int
    var str1: String
    var str2: String

    init?(end: String?,
          int1: String?,
          int2: String?,
          str1: String?,
          str2: String?) {
        guard let endString = end,
              let end = Int(endString),
              let int1String = int1,
              let int1 = Int(int1String),
              let int2String = int2,
              let int2 = Int(int2String),
              let str1 = str1,
              let str2 = str2 else {
            return nil
        }

        self.end = end
        self.int1 = int1
        self.int2 = int2
        self.str1 = str1
        self.str2 = str2
    }
    
    init(start: Int32,
          end: Int32,
          int1: Int32,
          int2: Int32,
          str1: String,
          str2: String) {
        self.start = Int(start)
        self.end = Int(end)
        self.int1 = Int(int1)
        self.int2 = Int(int2)
        self.str1 = str1
        self.str2 = str2
    }
    
    var format: String {
        return String(
            format: "(%d - %d) - %d - %@, %@",
            int1,
            int2,
            end,
            str1,
            str2)
    }
}
