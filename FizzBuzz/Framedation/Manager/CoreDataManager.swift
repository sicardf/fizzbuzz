//
//  CoreDataManager.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import UIKit
import CoreData

class CoreDataManager {
    
    enum CoreDataError: Error {
        case viewContext
        case loadData
    }
    
    static var shared = CoreDataManager()
    
    var moc: NSManagedObjectContext?
    var appDelegate: AppDelegate?
    
    init() {
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.moc = appDelegate?.persistentContainer.viewContext
    }
}

extension CoreDataManager {
    
    func saveFizzBuzzSearchEntity(with model: FizzBuzzModel) {
        guard let moc = moc else { return }

        let fizzBuzzSearchEntity = FizzBuzzSearchEntity(context: moc)
        
        fizzBuzzSearchEntity.start = Int32(model.start)
        fizzBuzzSearchEntity.end = Int32(model.end)
        fizzBuzzSearchEntity.int1 = Int32(model.int1)
        fizzBuzzSearchEntity.int2 = Int32(model.int2)
        fizzBuzzSearchEntity.str1 = model.str1
        fizzBuzzSearchEntity.str2 = model.str2
        
        appDelegate?.saveContext()
    }
    
    func getFizzBuzzSearchEntity() throws -> [FizzBuzzModel] {
        guard let moc = moc else {
            throw CoreDataError.viewContext
        }

        let fizzBuzzSearchRequest: NSFetchRequest<FizzBuzzSearchEntity> = FizzBuzzSearchEntity.fetchRequest()
        
        do {
            let fizzBuzzSearch = try moc.fetch(fizzBuzzSearchRequest)
            
            return fizzBuzzSearch.map { fizzBuzzSearchEntity in
                return FizzBuzzModel(
                    start: fizzBuzzSearchEntity.start,
                    end: fizzBuzzSearchEntity.end,
                    int1: fizzBuzzSearchEntity.int1,
                    int2: fizzBuzzSearchEntity.int2,
                    str1: fizzBuzzSearchEntity.str1 ?? "",
                    str2: fizzBuzzSearchEntity.str2 ?? "")
            }
        } catch {
            throw CoreDataError.loadData
        }
    }
}
