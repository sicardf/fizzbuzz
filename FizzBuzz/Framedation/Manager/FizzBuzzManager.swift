//
//  FizzBuzzGenerate.swift
//  FizzBuzz
//
//  Created by Flavien SICARD on 28/11/2021.
//

import Foundation

class FizzBuzzManager {

    func generateWith(_ model: FizzBuzzModel) -> [String] {
        var data = [String]()

        for offset in model.start...model.end {
            if let fizzBuzz = fizzbuzz(model, offset: offset) {
                data.append(fizzBuzz)
            }
        }

        return data
    }
}

private extension FizzBuzzManager {

    func fizzbuzz(_ model: FizzBuzzModel, offset: Int) -> String? {
        switch (offset % model.int1 == 0, offset % model.int2 == 0) {
        case (true, false):
            return model.str1
        case (false, true):
            return model.str2
        case (true, true):
            return model.str1 + model.str2
        default:
            return String(offset)
        }
    }
}
