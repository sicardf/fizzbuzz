//
//  FizzBuzzTests.swift
//  FizzBuzzTests
//
//  Created by Flavien SICARD on 28/11/2021.
//

import XCTest
@testable import FizzBuzz

class FizzBuzzTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testFizzBuzzGenerate() {
        guard let fizzBuzzModel = FizzBuzzModel(
            end: "10",
            int1: "3",
            int2: "5",
            str1: "fizz",
                str2: "buzz") else {
            XCTExpectFailure()

            return
        }

        let generate = FizzBuzzManager().generateWith(fizzBuzzModel)

        XCTAssertEqual(generate, ["1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz"])
    }
}
